######## The corporation #######################################################
#                                                                              #
#         █████╗       ██╗   ██╗███████╗██╗  ██╗████████╗ ██████╗ ██████╗      #
#        ██╔══██╗      ██║   ██║██╔════╝██║ ██╔╝╚══██╔══╝██╔═══██╗██╔══██╗     #
#        ███████║█████╗██║   ██║█████╗  █████╔╝    ██║   ██║   ██║██████╔╝     #
#        ██╔══██║╚════╝╚██╗ ██╔╝██╔══╝  ██╔═██╗    ██║   ██║   ██║██╔══██╗     #
#        ██║  ██║       ╚████╔╝ ███████╗██║  ██╗   ██║   ╚██████╔╝██║  ██║     #
#        ╚═╝  ╚═╝        ╚═══╝  ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝     #
#                                                                              #
#    (c) 2015 Avtomatika-Vektor                                                #
#                                                                              #
#                          A-Vektor Screen Saver                               #
#                                                                              #
################################################################################

TEMPLATE = app
TARGET = avss
INCLUDEPATH += . src
QT += opengl
QMAKE_CXXFLAGS += -mmmx -msse2 -msse3 -msse4.1 -m3dnow -std=gnu++11

# Input
HEADERS += src/AVLogo3D.h \
		   src/MainWidget.h

SOURCES += src/main.cpp \
		   src/AVLogo3D.cpp \
		   src/MainWidget.cpp

win32:RC_FILE += winres.rc
