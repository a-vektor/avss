/****** The corporation ********************************************************
 *                                                                             *
 *       █████╗       ██╗   ██╗███████╗██╗  ██╗████████╗ ██████╗ ██████╗       *
 *      ██╔══██╗      ██║   ██║██╔════╝██║ ██╔╝╚══██╔══╝██╔═══██╗██╔══██╗      *
 *      ███████║█████╗██║   ██║█████╗  █████╔╝    ██║   ██║   ██║██████╔╝      *
 *      ██╔══██║╚════╝╚██╗ ██╔╝██╔══╝  ██╔═██╗    ██║   ██║   ██║██╔══██╗      *
 *      ██║  ██║       ╚████╔╝ ███████╗██║  ██╗   ██║   ╚██████╔╝██║  ██║      *
 *      ╚═╝  ╚═╝        ╚═══╝  ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝      *
 *                                                                             *
 *  (c) 2015 Avtomatika-Vektor                                                 *
 *                                                                             *
 ******************************************************************************/

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QGLWidget>

class MainWidget : public QGLWidget
{
	Q_OBJECT

	private:
		qreal m_rotate_x,
			  m_rotate_y,
			  m_rotate_z;

	private Q_SLOTS:
		void animate();

	protected:
		void initializeGL() override final;
		void resizeGL( int w, int h ) override final;
		void paintGL() override final;

		void mouseMoveEvent( QMouseEvent * event ) override final;
		void mousePressEvent( QMouseEvent * event ) override final;
		void keyPressEvent( QKeyEvent * event ) override final;

	public:
		MainWidget( QWidget * parent = nullptr );
};

#endif

