/****** The corporation ********************************************************
 *                                                                             *
 *       █████╗       ██╗   ██╗███████╗██╗  ██╗████████╗ ██████╗ ██████╗       *
 *      ██╔══██╗      ██║   ██║██╔════╝██║ ██╔╝╚══██╔══╝██╔═══██╗██╔══██╗      *
 *      ███████║█████╗██║   ██║█████╗  █████╔╝    ██║   ██║   ██║██████╔╝      *
 *      ██╔══██║╚════╝╚██╗ ██╔╝██╔══╝  ██╔═██╗    ██║   ██║   ██║██╔══██╗      *
 *      ██║  ██║       ╚████╔╝ ███████╗██║  ██╗   ██║   ╚██████╔╝██║  ██║      *
 *      ╚═╝  ╚═╝        ╚═══╝  ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝      *
 *                                                                             *
 *  (c) 2015 Avtomatika-Vektor                                                 *
 *                                                                             *
 ******************************************************************************/

#include "MainWidget.h"

#include <QtGui>
//#include <QTimer>

#include "AVLogo3D.h"

MainWidget::MainWidget( QWidget * parent )
	: QGLWidget( QGLFormat( QGL::SampleBuffers ), parent ),
	m_rotate_x( .0 ), m_rotate_y( .0 ), m_rotate_z( .0 )
{
	setMouseTracking( true );	// mouseMoveEvent enable

	QTimer * timer = new QTimer( this );
	connect( timer, SIGNAL( timeout() ), SLOT( animate() ) );
	timer->start( 1000 / 60 );
}

void
MainWidget::initializeGL()
{
	qglClearColor( Qt::black );

	glEnable( GL_DEPTH_TEST );
}

void
MainWidget::resizeGL( int w, int h )
{
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	//glOrtho( -1000., 1000., -1000., 1000., -1., 1. );

#define	SIZE 1000.
#define HALF SIZE / 4.
	const qreal ratio = h / ( qreal ) w;

	glFrustum( -HALF, HALF, -HALF * ratio, HALF * ratio, SIZE * 3., SIZE * 20. );

	glViewport( 0, 0, ( GLint ) w, ( GLint ) h );
}

void
MainWidget::paintGL()
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glLoadIdentity();

	glTranslatef( 0, 0, -8500. );
	glRotatef( m_rotate_x, 1, 0, 0 );
	glRotatef( m_rotate_y, 0, 1, 0 );
	glRotatef( m_rotate_z, 0, 0, 1 );

	qglColor( QColor("#f0811f") );

	/*		крест в центре
	glBegin( GL_LINES );
#define CROSS 300.
	// hor
	glVertex3f( -CROSS, 0, 0 );
	glVertex3f( CROSS, 0, 0 );
	// ver
	glVertex3f( 0, -CROSS, 0 );
	glVertex3f( 0, CROSS, 0 );

	glEnd();
	*/

#define RULER( i ) glVertex3f( AVLogo3D::ruler[ i * 2 ] * 6., AVLogo3D::ruler[ i * 2 + 1 ] * 6., .0 );

	glBegin( GL_QUAD_STRIP );
	RULER( 0 )
	RULER( 3 )
	RULER( 1 )
	RULER( 4 )
	RULER( 2 )
	RULER( 5 )
	RULER( 0 )
	RULER( 3 )
	glEnd();

	qglColor( QColor("#175d3e") );

#define FIR( i ) glVertex3f( AVLogo3D::fir[ i * 2 ] * 6., AVLogo3D::fir[ i * 2 + 1 ] * 6., .0 );

	// нижняя хрень
	glBegin( GL_QUAD_STRIP );
	FIR( 0 )
	FIR( 5 )
	FIR( 1 )
	FIR( 4 )
	FIR( 2 )
	FIR( 3 )
	glEnd();
	// средняя хрень
	glBegin( GL_QUAD_STRIP );
	FIR( 6 )
	FIR( 11 )
	FIR( 7 )
	FIR( 10 )
	FIR( 8 )
	FIR( 9 )
	glEnd();
	// верхняя хрень
	glBegin( GL_QUAD_STRIP );
	FIR( 12 )
	FIR( 17 )
	FIR( 13 )
	FIR( 16 )
	FIR( 14 )
	FIR( 15 )
	glEnd();
}

void
MainWidget::mouseMoveEvent( QMouseEvent * event )
{
	close();
	event->accept();
}

void
MainWidget::mousePressEvent( QMouseEvent * event )
{
	close();
	event->accept();
}

void
MainWidget::keyPressEvent( QKeyEvent * event )
{
	close();
	event->accept();
}

void
MainWidget::animate()
{
	auto increase = []( qreal & var, qreal inc )->void
	{
		var += inc;
		if ( var > 360. )
			var -= 360.;
	};

	increase( m_rotate_x, .03 );
	increase( m_rotate_y, .2 );
	increase( m_rotate_z, .07 );

	update();
}

